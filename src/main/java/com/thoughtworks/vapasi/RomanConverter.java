package com.thoughtworks.vapasi;

public class RomanConverter {


    public Integer convertRomanToArabicNumber(String roman) {

        Integer result = 0;

        for (int i = 0; i < roman.length(); i++)
        {
            int currentValue = getRomanValue(roman.charAt(i));

            if (i + 1 < roman.length())
            {
                int nextValue = getRomanValue(roman.charAt(i+1));

                if (currentValue >= nextValue)
                {
                    result = result + currentValue;

                }
                else
                {
                    result = result + nextValue - currentValue;
                    i++;
                }

            }
            else {
                result = result + currentValue;
            }
        }
        return result;
    }
    public int getRomanValue(char c) {
    int result = 0;
    switch (c){
        case 'I':
            result =1;
            break;
        case 'V':
            result =5;
            break;
        case 'X' :
            result = 10;
            break;
        case 'L':
            result=50;
            break;
        case 'C':
            result=100;
            break;
        case 'D':
            result=500;
            break;
        case 'M':
            result=1000;
            break;

        default:
            result = -1;
    }
    return result;
    }

}